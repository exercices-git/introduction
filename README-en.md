# Git Basics Exercises

## Create a new repository

Create a folder called `exercice-git` on your disk

With a terminal like Git Bash, use the command that create a new git repository inside the `exercice-git` folder.

## Create an initial commit

Add a file called `contact.yaml` in the `exercice-git` folder the the following content :

```
contact:
  firstName: John
  lastName: Doe
  city: Paris
```

Add the changes to the staging area, then create a commit with message `chore: initial commit`

## Add new commits

Create to file in your project, `car.yaml` :

```
car:
  brand: Renault
  model: Clio
```

Then `company.yaml` :

```
company:
  name: Apple
  city: Cupertino
```

Create the files first and then create 2 different commits (one per file) with messages `feat: car.yaml` and `feat: company.yaml` 

## Add partial file to staging

In the `contact.yaml`, replace the name `John` with `Bob` and city `Paris` with `London`.

Create 2 commits (1 for each line) with messages : `fix(contact): firstName` and `fix(contact): city`

## Move files

Use the `git mv` to moves the 3 yaml files into a folder called `yaml` (you have to create the folder manually)

Commit the changes with the message : `refactor: move yaml files to yaml folder`.

## Rename last commit

Rename last commit using the command `git commit --amend`, the editor will launch, the message should be : `refactor: move all yaml files to yaml directory`.

## Delete a file

Use the `git rm` to delete the `car.yaml`

Create 1 commit with message : `refactor!: delete car.yaml`.

With zsh or bash (therefore with Git Bash), we need to escape the character `!` : `git commit -m "refactor\!: delete voiture.yaml`


## Go back in history

Use the `git reset HEAD~1 --hard` to cancel the last commit, the `car.yaml` file should be restored.

## Show the differences

Use the `git show` to show the patch of the last commit

Use the `git diff` with the right arguments to show the content of the last two commits concerning `contact.yaml` (the ones created with `git add -p`)
